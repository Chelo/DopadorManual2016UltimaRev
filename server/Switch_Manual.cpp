#include "stdafx.h"
#include "config.h"
#include "constants.h"
#include "utils.h"
#include "log.h"
#include "char.h"
#include "dev_log.h"
#include "locale_service.h"
#include "item.h"
#include "item_manager.h"
bool	ValidadObjecto(LPITEM item){
	if (!item) return false;
	switch (item->GetType()){
		case ITEM_WEAPON:
		case ITEM_ARMOR:
		case ARMOR_BODY:
			if (item->IsEquipped() || item->IsExchanging())
				return false;
			return true;
		default:
			return false;
	}
}
void	EntradaPrincipal(LPCHARACTER ch, TItemPos Cell, BYTE acction, BYTE sub_acction){
	if (ch == NULL) return;
	LPITEM item = ch->GetItem(Cell);
	if (item == NULL){
		ch->ChatPacket(CHAT_TYPE_INFO, "El objeto seleccionado no es valido");
		return;
	}
	if (ValidadObjecto(item) == false){
		ch->ChatPacket(CHAT_TYPE_INFO, "El objeto seleccionado no es valido");
		return;
	}
	if (acction >= 1 && acction <= 2){
		if (sub_acction >= 1 && sub_acction <= 2){
			//Normales.
			if (acction == 1){
				if (chelo_dopador_normal_part1 == false)
					return;
				if (sub_acction == 1){Normal_AgregarBonus(ch, item);}
					
				else if (sub_acction == 2){Normal_CambiarBonus(ch, item);}
					
			}
			else if (acction == 2){
				if (chelo_dopador_normal_part2 == false)
					return;
				if (sub_acction == 1){Especial_AgregarBonus(ch, item);}
					
				else if (sub_acction == 2){Especial_CambiarBonus(ch, item);}
					
			}
		}
		else{
			ch->ChatPacket(CHAT_TYPE_INFO, "La acci�n seleccionada no es valida.");
		}
	}
	else{
		ch->ChatPacket(CHAT_TYPE_INFO, "La acci�n seleccionada no es valida.");
	}
}
void Normal_AgregarBonus(LPCHARACTER ch, LPITEM item){
	if (ch == NULL || item == NULL) return;
	if (item->GetAttributeCount() == 5){
		ch->ChatPacket(CHAT_TYPE_INFO, "No puedes agregar m�s attributos a est� objeto.");
		return;
	}
	if (ch->Normal_EstadoAgregar == true){
		if (ch->Normal_AgregarBonus_c >= 1){
			if(cost_yang_add_bonus_normal){
				if(ch->GetGold() >= cost_yang_add_bonus_normal_cant){
					ch->PointChange(POINT_GOLD, -cost_yang_add_bonus_normal_cant);
				}else{
					
					ch->ChatPacket(CHAT_TYPE_INFO, "No tienes suficiente yang.");
					return;
				}
			}
			ch->Normal_AgregarBonus_c -=1;
			item->AddAttribute();
			if (ch->Normal_AgregarBonus_c <= 0){
				ch->Normal_EstadoAgregar = false;
				ch->Code_normal_a1 = number(1000,9999);
				ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_C2 %d",ch->Code_normal_a1);
			}
			ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_A1 %d",ch->Normal_AgregarBonus_c);
		}
		
	}	
}
void Normal_CambiarBonus(LPCHARACTER ch, LPITEM item){
	if (ch == NULL || item == NULL) return;
	if (item->GetAttributeSetIndex() == -1 || item->GetAttributeCount() == 0)
		return;
	if (ch->Normal_EstadoCambio == true){
		if (ch->Normal_CambiosDisponibles >= 1){
			if(cost_yang_can_bonus_normal){
				if(ch->GetGold() >= cost_yang_can_bonus_normal_cant){
					ch->PointChange(POINT_GOLD, -cost_yang_can_bonus_normal_cant);
				}else{
					
					ch->ChatPacket(CHAT_TYPE_INFO, "No tienes suficiente yang.");
					return;
				}
			}
			ch->Normal_CambiosDisponibles -= 1;
			item->ChangeAttribute();
			if (ch->Normal_CambiosDisponibles <= 0){
				ch->Normal_EstadoCambio = false;
				ch->Code_normal_c1 = number(1000,9999);
				ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_C1 %d",ch->Code_normal_c1);
			}
			ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_C1 %d",ch->Normal_CambiosDisponibles);
		}
	}
	
}
void Especial_AgregarBonus(LPCHARACTER ch, LPITEM item){
	if (ch == NULL || item == NULL) return;
	if (item->GetAttributeCount() >= 5 && item->GetAttributeCount() <= 7){
		if (ch->Especial_EstadoAgregar == true){
			if (ch->Especial_AgregarBonus_c >= 1){
				if(cost_yang_add_bonus_especial){
					if(ch->GetGold() >= cost_yang_add_bonus_especial_cant){
						ch->PointChange(POINT_GOLD, -cost_yang_add_bonus_especial_cant);
					}else{
						
						ch->ChatPacket(CHAT_TYPE_INFO, "No tienes suficiente yang.");
						return;
					}
				}
				ch->Especial_AgregarBonus_c -= 1;
				item->AddRareAttribute();
				if (ch->Especial_AgregarBonus_c <= 0){
					ch->Especial_EstadoAgregar = false;
					ch->Code_Especial_a1 = number(1000, 9999);
					ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_CE2 %d",ch->Code_Especial_a1);
				}
				ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_A2 %d",ch->Especial_AgregarBonus_c);
			}
		} 
		
	}
	else{
		ch->ChatPacket(CHAT_TYPE_INFO, "Para poder a�adir los bonus especiales debes agregar los primero atributos.");
		return;
	}
}
void Especial_CambiarBonus(LPCHARACTER ch, LPITEM item)
{
	if (ch == NULL || item == NULL) return;
	if (item->GetAttributeCount() >= 5){
		if (ch->Especial_EstadoCambio == true){
			if (ch->Especial_CambiosDisponibles >= 1){
				if(cost_yang_add_bonus_especial){
					if(ch->GetGold() >= cost_yang_add_bonus_especial_cant){
						ch->PointChange(POINT_GOLD, -cost_yang_add_bonus_especial_cant);
					}else{
						
						ch->ChatPacket(CHAT_TYPE_INFO, "No tienes suficiente yang.");
						return;
					}
				}
				ch->Especial_CambiosDisponibles -= 1;
				item->ChangeRareAttribute();
				if (ch->Especial_CambiosDisponibles <= 0){
					ch->Especial_EstadoCambio = false;
					ch->Code_Especial_c1 = number(1000, 9999);
					ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_CE1 %d",ch->Code_Especial_c1);
				}
				ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_C2 %d",ch->Especial_CambiosDisponibles);
			}
		}
	}
	else{
		ch->ChatPacket(CHAT_TYPE_INFO, "No hay bonus para cambiar.");
		return;
	}
}
void	SwitchCode(LPCHARACTER ch, int code, BYTE acction, BYTE sub_acction){
	if (ch == NULL) return;
	if (acction == 1){
		if(chelo_dopador_normal_part1 == false)
			return;
		if (sub_acction == 1){
			if (ch->Normal_EstadoCambio == false){
				if (code == ch->Code_normal_c1){
					ch->Normal_EstadoCambio = true;
					ch->Normal_CambiosDisponibles = VALOR_SYSTEM_PART1_CHANG;
					ch->Code_normal_c1 = 0;
				}
			}
		}else if (sub_acction == 2){
			if (ch->Normal_EstadoAgregar == false){
				if (code == ch->Code_normal_a1){
					ch->Normal_EstadoAgregar = true;
					ch->Normal_AgregarBonus_c = VALOR_SYSTEM_PART1_ADD;
					ch->Code_normal_a1 = 0;
				}
			}
		}
	}else if (acction == 2){
		if(chelo_dopador_normal_part2 == false)
			return;
		if (sub_acction == 1){
			if (ch->Especial_EstadoCambio == false){
				if (code == ch->Code_Especial_c1){
					ch->Especial_EstadoCambio = true;
					ch->Especial_CambiosDisponibles = VALOR_SYSTEM_PART2_CHANG;
					ch->Code_Especial_c1 = 0;
				}
			}
		}else if (sub_acction == 2){
			if (ch->Especial_EstadoAgregar == false){
				if (code == ch->Code_Especial_a1){
					ch->Especial_EstadoAgregar = true;
					ch->Especial_AgregarBonus_c = VALOR_SYSTEM_PART2_ADD;
					ch->Code_Especial_a1 = 0;
				}
			}
		}
	}
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_C1 %d",ch->Normal_CambiosDisponibles);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_C2 %d",ch->Especial_CambiosDisponibles);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_A1 %d",ch->Normal_AgregarBonus_c);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_A2 %d",ch->Especial_AgregarBonus_c);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_C2 %d",ch->Code_normal_a1);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_C1 %d",ch->Code_normal_c1);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_CE2 %d",ch->Code_Especial_a1);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_CE1 %d",ch->Code_Especial_c1);
}
//Test Function
void 	RestartAllSystem(LPCHARACTER ch){
	if (ch ==NULL)return;
	ch->Normal_CambiosDisponibles = VALOR_SYSTEM_PART1_CHANG;
	ch->Normal_EstadoCambio = true;
	ch->Especial_CambiosDisponibles = VALOR_SYSTEM_PART2_CHANG;
	ch->Especial_EstadoCambio = true;
	ch->Normal_AgregarBonus_c = VALOR_SYSTEM_PART1_ADD;
	ch->Normal_EstadoAgregar = true;
	ch->Especial_AgregarBonus_c = VALOR_SYSTEM_PART2_ADD;
	ch->Especial_EstadoAgregar = true;
	ch->Code_normal_c1 = 0;
	ch->Code_normal_a1 = 0;
	ch->Code_Especial_c1 = 0;
	ch->Code_Especial_a1 = 0;
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_C1 %d",ch->Normal_CambiosDisponibles);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_C2 %d",ch->Especial_CambiosDisponibles);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_A1 %d",ch->Normal_AgregarBonus_c);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_A2 %d",ch->Especial_AgregarBonus_c);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_C2 %d",ch->Code_normal_a1);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_C1 %d",ch->Code_normal_c1);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_CE2 %d",ch->Code_Especial_a1);
	ch->ChatPacket(CHAT_TYPE_COMMAND, "CHELO_DOPADOR_MANUAL_CODE_CE1 %d",ch->Code_Especial_c1);
}


