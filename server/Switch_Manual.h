#ifndef HEADER_SWITCH_MANUAL
#define HEADER_SWITCH_MANUAL
bool	ValidadObjecto(LPITEM item);
void	EntradaPrincipal(LPCHARACTER ch, TItemPos Cell, BYTE acction, BYTE sub_acction);
void	Normal_AgregarBonus(LPCHARACTER ch, LPITEM item);
void	Normal_CambiarBonus(LPCHARACTER ch, LPITEM item);
void	Especial_AgregarBonus(LPCHARACTER ch, LPITEM item);
void	Especial_CambiarBonus(LPCHARACTER ch, LPITEM item);
void 	RestartAllSystem(LPCHARACTER ch);
void	SwitchCode(LPCHARACTER ch, int code, BYTE acction, BYTE sub_acction);
#endif