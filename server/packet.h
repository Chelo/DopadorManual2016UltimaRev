//Dentro de la lista de los headers.
HEADER_GC_ITEM_SWITCH			= 58,
HEADER_GC_ITEM_SWITCH_CODE			= 57,
//Al final antes de #pragma pack()
typedef struct SPacketSwitch{
	BYTE header;
	TItemPos Cell;
	BYTE acction;
	BYTE sub_acction;
}TPacketSwitchItem;
typedef struct SPacketSwitchCode{
	BYTE header;
	int code;
	BYTE acction;
	BYTE sub_acction;
}TPacketSwitchItemCode;