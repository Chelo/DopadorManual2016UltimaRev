//Arriba de int CInputDead::Analyze(LPDESC d, BYTE bHeader, const char * c_pData)
void CInputMain::SwitchAcction(LPCHARACTER ch, const char * data)
{
	struct SPacketSwitch * pinfo = (struct SPacketSwitch *) data;
	if (ch)
		EntradaPrincipal(ch,pinfo->Cell, pinfo->acction, pinfo->sub_acction);
}
void CInputMain::SwitchAcctionCode(LPCHARACTER ch, const char * data)
{
	struct SPacketSwitchCode * pinfo = (struct SPacketSwitchCode *) data;
	if (ch)
		SwitchCode(ch,pinfo->code, pinfo->acction, pinfo->sub_acction);
}
//Abajo de case HEADER_CG_DRAGON_SOUL_REFINE:{} break;
case HEADER_GC_ITEM_SWITCH:
	if (!ch->IsObserverMode())
		SwitchAcction(ch, c_pData);
	break;
case HEADER_GC_ITEM_SWITCH_CODE:
	if (!ch->IsObserverMode())
		SwitchAcctionCode(ch, c_pData);
	break;