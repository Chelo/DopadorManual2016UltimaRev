#################################################
#		Switch developed chelo Version 2.5		#
#################################################
BOARD_WIDTH = 220
BOARD_HEIGHT = 450
window = {
	"name" : "SwitchChelo25",
	"x" : (SCREEN_WIDTH / 2) - (BOARD_WIDTH / 2) ,
	"y" : (SCREEN_HEIGHT /2 ) - (BOARD_HEIGHT / 2),
 
	"style" : ("movable", "float",),
 
	"width" : BOARD_WIDTH,
	"height" : BOARD_HEIGHT,
	
	"children" :
	(
		#Board
		{
			"name" : "board",
			"type" : "board",
			"style" : ("attach",),
 
			"x" : 0,
			"y" : 0,
 
			"width" :BOARD_WIDTH,
			"height" : BOARD_HEIGHT,
 
			"children" :
			(
				#TitleBar
				{
					"name" : "TitleBar",
					"type" : "titlebar",
					"style" : ("attach",),
 
					"x" : 8,
					"y" : 7,
 
					"width" : BOARD_WIDTH -15,
					"color" : "yellow",
 
					"children" :
					(
						{ "name":"TitleName", "type":"text", "x":BOARD_WIDTH / 2 - 10, "y":3, "text":"Cambiador de Bonus", "text_horizontal_align":"center" },
					),
				},
				{
					"name" : "ItemSlot",
					"type" : "grid_table",

					"x" : 15,
					"y" : 60,

					"start_index" : 0,
					"x_count" : 1,
					"y_count" : 3,
					"x_step" : 32,
					"y_step" : 32,

					"image" : "d:/ymir work/ui/public/Slot_Base.sub"
				},
				{
					"name" : "CopyRight",
					"type" : "horizontalbar",
					"x" : 5,
					"y" : BOARD_HEIGHT / 3 + 20,
					"width" : BOARD_WIDTH - 13,
					"children" :
					(
						{
							"name" : "(C)",
							"type" : "text",
							"x" : 0,
							"y" : 0,
							"all_align" : "center",
							"text" : "Bonus - Modo normal",
							"r" : 1.0,
							"g" : 0.0,
							"b" : 0.0,
							"a" : 1.0,
						},
					),
				},
				{
					"name" : "CambiosDisponibles",
					"type" : "text",
					"x" : 0,
					"y" : -150,
					"all_align" : "center",
					"text" : "Cambios disponibles 0",
				},
				{
					"name" : "AgregarDisponibles",
					"type" : "text",
					"x" : 0,
					"y" : -130,
					"all_align" : "center",
					"text" : "Agregar disponibles 0",
				},
				{
					"name" : "thinboard_bonus1",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_1",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "thinboard_bonus2",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3 + 30,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_2",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "thinboard_bonus3",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3 + 30 + 30,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_3",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "thinboard_bonus4",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3 + 30+30+30,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_4",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "thinboard_bonus5",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3 + 30 + 30 + 30 + 30,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_5",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "thinboard_bonus6",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3 + 30 + 30 + 30 + 30 + 30,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_6",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "thinboard_bonus7",
					"type" : "thinboard",
					
					"x" : 7,
					"y" : BOARD_HEIGHT / 3 + 40 - 3 + 30 + 30 + 30 + 30 +30+30,
					"width" : BOARD_WIDTH - 13,
					"height" : 30,
					"children" :
					(
						{
							"name" : "thin_text_7",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "None",
						},
					),
				},
				{
					"name" : "BTN_NORMAL",
					"type" : "radio_button",
 
					"x" : 5,
					"y" : 30,
 
					"default_image" : "d:/ymir work/ui/game/windows/tab_button_small_01.sub",
					"over_image" : "d:/ymir work/ui/game/windows/tab_button_small_02.sub",
					"down_image" : "d:/ymir work/ui/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "Modo normal 1/5 bonus",
 
					"children" :
					(
						{
							"name" : "Inventory_Tab_04_Print",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "N",
						},
					),
				}, 
				{
					"name" : "BTN_ESPECIAL",
					"type" : "radio_button",
 
					"x" : 180,
					"y" : 30,
 
					"default_image" : "d:/ymir work/ui/game/windows/tab_button_small_01.sub",
					"over_image" : "d:/ymir work/ui/game/windows/tab_button_small_02.sub",
					"down_image" : "d:/ymir work/ui/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "Modo especial 6/7 bonus",
 
					"children" :
					(
						{
							"name" : "Inventory_Tab_04_Print",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "E",
						},
					),
				}, 
				{
					"name" : "DeleteItem",
					"type" : "button",
 
					"x" : BOARD_WIDTH /2 - 100,
					"y" : BOARD_HEIGHT - 50 + 10,
 
					"default_image" : "d:/ymir work/ui/game/windows/tab_button_small_01.sub",
					"over_image" : "d:/ymir work/ui/game/windows/tab_button_small_02.sub",
					"down_image" : "d:/ymir work/ui/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "Eliminar Objeto",
 
					"children" :
					(
						{
							"name" : "Inventory_Tab_04_Print",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "X",
						},
					),
				},
				{
					"name" : "AddBonus",
					"type" : "button",
 
					"x" : BOARD_WIDTH /2 - 18,
					"y" : BOARD_HEIGHT - 50 +10,
 
					"default_image" : "d:/ymir work/ui/game/windows/tab_button_small_01.sub",
					"over_image" : "d:/ymir work/ui/game/windows/tab_button_small_02.sub",
					"down_image" : "d:/ymir work/ui/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "Agregar Bonus",
 
					"children" :
					(
						{
							"name" : "Inventory_Tab_04_Print",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "A",
						},
					),
				},
				{
					"name" : "ChangeBonus",
					"type" : "button",
 
					"x" : BOARD_WIDTH /2 + 65,
					"y" : BOARD_HEIGHT - 50 +10,
 
					"default_image" : "d:/ymir work/ui/game/windows/tab_button_small_01.sub",
					"over_image" : "d:/ymir work/ui/game/windows/tab_button_small_02.sub",
					"down_image" : "d:/ymir work/ui/game/windows/tab_button_small_03.sub",
					"tooltip_text" : "Cambiar Bonus",
 
					"children" :
					(
						{
							"name" : "Inventory_Tab_04_Print",
							"type" : "text",
 
							"x" : 0,
							"y" : 0,
 
							"all_align" : "center",
 
							"text" : "C",
						},
					),
				},
				{
					"name" : "Btn_Add_Valur1",
					"type" : "button",
 
					"x" : 168,
					"y" : 68,
 
					"default_image" : "d:/ymir work/ui/game/windows/btn_plus_dis.sub",
					"over_image" : "d:/ymir work/ui/game/windows/btn_plus_over.sub",
					"down_image" : "d:/ymir work/ui/game/windows/btn_plus_down.sub",
				},
				{
					"name" : "Btn_Add_Valur2",
					"type" : "button",
 
					"x" : 168,
					"y" : 90,
 
					"default_image" : "d:/ymir work/ui/game/windows/btn_plus_dis.sub",
					"over_image" : "d:/ymir work/ui/game/windows/btn_plus_over.sub",
					"down_image" : "d:/ymir work/ui/game/windows/btn_plus_down.sub",
				},
			),
		},
	),
}