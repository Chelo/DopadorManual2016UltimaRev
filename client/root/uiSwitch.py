#################################################
#		Switch developed chelo Version 2.5		#
#################################################
import ui
import exception
import mouseModule
import player
import grp
import localeInfo
import item
import net
import constInfo
class SwitchChelo(ui.ScriptWindow):
	POSITIVE_COLOR = grp.GenerateColor(0.5411, 0.7254, 0.5568, 1.0)
	NEGATIVE_COLOR = grp.GenerateColor(0.9, 0.4745, 0.4627, 1.0)
	AFFECT_DICT = {
		item.APPLY_MAX_HP : localeInfo.TOOLTIP_MAX_HP,
		item.APPLY_MAX_SP : localeInfo.TOOLTIP_MAX_SP,
		item.APPLY_CON : localeInfo.TOOLTIP_CON,
		item.APPLY_INT : localeInfo.TOOLTIP_INT,
		item.APPLY_STR : localeInfo.TOOLTIP_STR,
		item.APPLY_DEX : localeInfo.TOOLTIP_DEX,
		item.APPLY_ATT_SPEED : localeInfo.TOOLTIP_ATT_SPEED,
		item.APPLY_MOV_SPEED : localeInfo.TOOLTIP_MOV_SPEED,
		item.APPLY_CAST_SPEED : localeInfo.TOOLTIP_CAST_SPEED,
		item.APPLY_HP_REGEN : localeInfo.TOOLTIP_HP_REGEN,
		item.APPLY_SP_REGEN : localeInfo.TOOLTIP_SP_REGEN,
		item.APPLY_POISON_PCT : localeInfo.TOOLTIP_APPLY_POISON_PCT,
		item.APPLY_STUN_PCT : localeInfo.TOOLTIP_APPLY_STUN_PCT,
		item.APPLY_SLOW_PCT : localeInfo.TOOLTIP_APPLY_SLOW_PCT,
		item.APPLY_CRITICAL_PCT : localeInfo.TOOLTIP_APPLY_CRITICAL_PCT,
		item.APPLY_PENETRATE_PCT : localeInfo.TOOLTIP_APPLY_PENETRATE_PCT,

		item.APPLY_ATTBONUS_WARRIOR : localeInfo.TOOLTIP_APPLY_ATTBONUS_WARRIOR,
		item.APPLY_ATTBONUS_ASSASSIN : localeInfo.TOOLTIP_APPLY_ATTBONUS_ASSASSIN,
		item.APPLY_ATTBONUS_SURA : localeInfo.TOOLTIP_APPLY_ATTBONUS_SURA,
		item.APPLY_ATTBONUS_SHAMAN : localeInfo.TOOLTIP_APPLY_ATTBONUS_SHAMAN,
		item.APPLY_ATTBONUS_MONSTER : localeInfo.TOOLTIP_APPLY_ATTBONUS_MONSTER,

		item.APPLY_ATTBONUS_HUMAN : localeInfo.TOOLTIP_APPLY_ATTBONUS_HUMAN,
		item.APPLY_ATTBONUS_ANIMAL : localeInfo.TOOLTIP_APPLY_ATTBONUS_ANIMAL,
		item.APPLY_ATTBONUS_ORC : localeInfo.TOOLTIP_APPLY_ATTBONUS_ORC,
		item.APPLY_ATTBONUS_MILGYO : localeInfo.TOOLTIP_APPLY_ATTBONUS_MILGYO,
		item.APPLY_ATTBONUS_UNDEAD : localeInfo.TOOLTIP_APPLY_ATTBONUS_UNDEAD,
		item.APPLY_ATTBONUS_DEVIL : localeInfo.TOOLTIP_APPLY_ATTBONUS_DEVIL,
		item.APPLY_STEAL_HP : localeInfo.TOOLTIP_APPLY_STEAL_HP,
		item.APPLY_STEAL_SP : localeInfo.TOOLTIP_APPLY_STEAL_SP,
		item.APPLY_MANA_BURN_PCT : localeInfo.TOOLTIP_APPLY_MANA_BURN_PCT,
		item.APPLY_DAMAGE_SP_RECOVER : localeInfo.TOOLTIP_APPLY_DAMAGE_SP_RECOVER,
		item.APPLY_BLOCK : localeInfo.TOOLTIP_APPLY_BLOCK,
		item.APPLY_DODGE : localeInfo.TOOLTIP_APPLY_DODGE,
		item.APPLY_RESIST_SWORD : localeInfo.TOOLTIP_APPLY_RESIST_SWORD,
		item.APPLY_RESIST_TWOHAND : localeInfo.TOOLTIP_APPLY_RESIST_TWOHAND,
		item.APPLY_RESIST_DAGGER : localeInfo.TOOLTIP_APPLY_RESIST_DAGGER,
		item.APPLY_RESIST_BELL : localeInfo.TOOLTIP_APPLY_RESIST_BELL,
		item.APPLY_RESIST_FAN : localeInfo.TOOLTIP_APPLY_RESIST_FAN,
		item.APPLY_RESIST_BOW : localeInfo.TOOLTIP_RESIST_BOW,
		item.APPLY_RESIST_FIRE : localeInfo.TOOLTIP_RESIST_FIRE,
		item.APPLY_RESIST_ELEC : localeInfo.TOOLTIP_RESIST_ELEC,
		item.APPLY_RESIST_MAGIC : localeInfo.TOOLTIP_RESIST_MAGIC,
		item.APPLY_RESIST_WIND : localeInfo.TOOLTIP_APPLY_RESIST_WIND,
		item.APPLY_REFLECT_MELEE : localeInfo.TOOLTIP_APPLY_REFLECT_MELEE,
		item.APPLY_REFLECT_CURSE : localeInfo.TOOLTIP_APPLY_REFLECT_CURSE,
		item.APPLY_POISON_REDUCE : localeInfo.TOOLTIP_APPLY_POISON_REDUCE,
		item.APPLY_KILL_SP_RECOVER : localeInfo.TOOLTIP_APPLY_KILL_SP_RECOVER,
		item.APPLY_EXP_DOUBLE_BONUS : localeInfo.TOOLTIP_APPLY_EXP_DOUBLE_BONUS,
		item.APPLY_GOLD_DOUBLE_BONUS : localeInfo.TOOLTIP_APPLY_GOLD_DOUBLE_BONUS,
		item.APPLY_ITEM_DROP_BONUS : localeInfo.TOOLTIP_APPLY_ITEM_DROP_BONUS,
		item.APPLY_POTION_BONUS : localeInfo.TOOLTIP_APPLY_POTION_BONUS,
		item.APPLY_KILL_HP_RECOVER : localeInfo.TOOLTIP_APPLY_KILL_HP_RECOVER,
		item.APPLY_IMMUNE_STUN : localeInfo.TOOLTIP_APPLY_IMMUNE_STUN,
		item.APPLY_IMMUNE_SLOW : localeInfo.TOOLTIP_APPLY_IMMUNE_SLOW,
		item.APPLY_IMMUNE_FALL : localeInfo.TOOLTIP_APPLY_IMMUNE_FALL,
		item.APPLY_BOW_DISTANCE : localeInfo.TOOLTIP_BOW_DISTANCE,
		item.APPLY_DEF_GRADE_BONUS : localeInfo.TOOLTIP_DEF_GRADE,
		item.APPLY_ATT_GRADE_BONUS : localeInfo.TOOLTIP_ATT_GRADE,
		item.APPLY_MAGIC_ATT_GRADE : localeInfo.TOOLTIP_MAGIC_ATT_GRADE,
		item.APPLY_MAGIC_DEF_GRADE : localeInfo.TOOLTIP_MAGIC_DEF_GRADE,
		item.APPLY_MAX_STAMINA : localeInfo.TOOLTIP_MAX_STAMINA,
		item.APPLY_MALL_ATTBONUS : localeInfo.TOOLTIP_MALL_ATTBONUS,
		item.APPLY_MALL_DEFBONUS : localeInfo.TOOLTIP_MALL_DEFBONUS,
		item.APPLY_MALL_EXPBONUS : localeInfo.TOOLTIP_MALL_EXPBONUS,
		item.APPLY_MALL_ITEMBONUS : localeInfo.TOOLTIP_MALL_ITEMBONUS,
		item.APPLY_MALL_GOLDBONUS : localeInfo.TOOLTIP_MALL_GOLDBONUS,
		item.APPLY_SKILL_DAMAGE_BONUS : localeInfo.TOOLTIP_SKILL_DAMAGE_BONUS,
		item.APPLY_NORMAL_HIT_DAMAGE_BONUS : localeInfo.TOOLTIP_NORMAL_HIT_DAMAGE_BONUS,
		item.APPLY_SKILL_DEFEND_BONUS : localeInfo.TOOLTIP_SKILL_DEFEND_BONUS,
		item.APPLY_NORMAL_HIT_DEFEND_BONUS : localeInfo.TOOLTIP_NORMAL_HIT_DEFEND_BONUS,
		item.APPLY_PC_BANG_EXP_BONUS : localeInfo.TOOLTIP_MALL_EXPBONUS_P_STATIC,
		item.APPLY_PC_BANG_DROP_BONUS : localeInfo.TOOLTIP_MALL_ITEMBONUS_P_STATIC,
		item.APPLY_RESIST_WARRIOR : localeInfo.TOOLTIP_APPLY_RESIST_WARRIOR,
		item.APPLY_RESIST_ASSASSIN : localeInfo.TOOLTIP_APPLY_RESIST_ASSASSIN,
		item.APPLY_RESIST_SURA : localeInfo.TOOLTIP_APPLY_RESIST_SURA,
		item.APPLY_RESIST_SHAMAN : localeInfo.TOOLTIP_APPLY_RESIST_SHAMAN,
		item.APPLY_MAX_HP_PCT : localeInfo.TOOLTIP_APPLY_MAX_HP_PCT,
		item.APPLY_MAX_SP_PCT : localeInfo.TOOLTIP_APPLY_MAX_SP_PCT,
		item.APPLY_ENERGY : localeInfo.TOOLTIP_ENERGY,
		item.APPLY_COSTUME_ATTR_BONUS : localeInfo.TOOLTIP_COSTUME_ATTR_BONUS,
		
		item.APPLY_MAGIC_ATTBONUS_PER : localeInfo.TOOLTIP_MAGIC_ATTBONUS_PER,
		item.APPLY_MELEE_MAGIC_ATTBONUS_PER : localeInfo.TOOLTIP_MELEE_MAGIC_ATTBONUS_PER,
		item.APPLY_RESIST_ICE : localeInfo.TOOLTIP_RESIST_ICE,
		item.APPLY_RESIST_EARTH : localeInfo.TOOLTIP_RESIST_EARTH,
		item.APPLY_RESIST_DARK : localeInfo.TOOLTIP_RESIST_DARK,
		item.APPLY_ANTI_CRITICAL_PCT : localeInfo.TOOLTIP_ANTI_CRITICAL_PCT,
		item.APPLY_ANTI_PENETRATE_PCT : localeInfo.TOOLTIP_ANTI_PENETRATE_PCT,
		item.APPLY_ATTBONUS_WOLFMAN : localeInfo.TOOLTIP_APPLY_ATTBONUS_WOLFMAN,
		item.APPLY_RESIST_WOLFMAN : localeInfo.TOOLTIP_APPLY_RESIST_WOLFMAN,
		item.APPLY_RESIST_CLAW : localeInfo.TOOLTIP_APPLY_RESIST_CLAW,
		item.APPLY_BLEEDING_PCT : localeInfo.TOOLTIP_APPLY_BLEEDING_PCT,
		item.APPLY_BLEEDING_REDUCE : localeInfo.TOOLTIP_APPLY_BLEEDING_REDUCE,
	}
	def __init__(self):
		ui.ScriptWindow.__init__(self)
		self.LoadBuildWindow()
		
	def __del__(self):
		ui.ScriptWindow.__del__(self)
		
	def Close(self):
		self.EliminarObjeto()
		self.Hide()
		
	def LoadBuildWindow(self):
		try:
			pyScrLoader = ui.PythonScriptLoader()
			pyScrLoader.LoadScriptFile(self, "uiscript/Switch.py")
		except:
			import exception
			exception.Abort("SwitchChelo25.LoadWindow.LoadObject")
			
		#LoadWindow
		try:
			self.GetChild("TitleBar").SetCloseEvent(ui.__mem_func__(self.Close))
			btn_normal = self.GetChild("BTN_NORMAL")
			btn_especial = self.GetChild("BTN_ESPECIAL")
			item_Slot = self.GetChild("ItemSlot")
		except:
			import exception
			exception.Abort("SwitchChelo25.LoadWindow.LoadObject")
			
		btn_normal.SetEvent(lambda arg = 1: self.cambiarmodo(arg))
		btn_especial.SetEvent(lambda arg = 2: self.cambiarmodo(arg))
		item_Slot.SetSelectEmptySlotEvent(ui.__mem_func__(self.__OnSelectEmptySlot))
		self.GetChild("DeleteItem").SetEvent(self.EliminarObjeto)
		self.GetChild("AddBonus").SetEvent(lambda arg = 1:self.BonusChange(arg))
		self.GetChild("ChangeBonus").SetEvent(lambda arg = 2:self.BonusChange(arg))
		self.GetChild("Btn_Add_Valur1").SetEvent(lambda arg = 1:self.CodigoOpen(arg))
		self.GetChild("Btn_Add_Valur2").SetEvent(lambda arg = 2:self.CodigoOpen(arg))
		self.btn_normal = btn_normal
		self.btn_especial = btn_especial
		self.item_Slot = item_Slot
		self.ConfigDefault()
	
	def CodigoOpen(self, type):
		servercode = 0
		if self.TipoSistema == 1:
			if type == 1:
				servercode = constInfo.CODIGO_SERVER_C1
			elif type == 2:
				servercode = constInfo.CODIGO_SERVER_C2
				
		elif self.TipoSistema == 2:
			if type == 1:
				servercode = constInfo.CODIGO_SERVER_CE1
			elif type == 2:
				servercode = constInfo.CODIGO_SERVER_CE2
		self.type = type
		self.BoardCodigo(servercode)
		
			
	def BoardCodigo(self, servercodes):
		self.BoardCode = ui.BoardWithTitleBar()
		self.BoardCode.SetTitleName("Codigo de seguridad")
		self.BoardCode.SetSize(200,130)
		self.BoardCode.AddFlag("movable")
		self.BoardCode.AddFlag("attach")
		self.BoardCode.SetCenterPosition()
		self.BoardCode.Show()
		
		self.servercode = ui.SlotBar()
		self.servercode.SetParent(self.BoardCode)
		self.servercode.SetSize(90 + 67, 18)
		self.servercode.SetPosition(200 / 2 - 90 + 67 / 2 - 25,35)
		self.servercode.Show()
		
		self.servergetcode = ui.TextLine()
		self.servergetcode.SetParent(self.servercode)
		self.servergetcode.SetSize(90 + 67,18)
		self.servergetcode.SetPosition(70,3)
		self.servergetcode.SetText(servercodes)
		self.servergetcode.Show()
		
		self.mycode = ui.SlotBar()
		self.mycode.SetParent(self.BoardCode)
		self.mycode.SetSize(90 + 67, 18)
		self.mycode.SetPosition(200 / 2 - 90 + 67 / 2 - 25,35 + 18 + 4)
		self.mycode.Show()
		
		self.mycodeget = ui.EditLine()
		self.mycodeget.SetParent(self.mycode)
		self.mycodeget.SetSize(90 + 67,18)
		self.mycodeget.SetMax(4)
		self.mycodeget.SetPosition(70,3)
		self.mycodeget.SetText("")
		self.mycodeget.Show()
		
		self.SetButton = ui.Button()
		self.SetButton.SetParent(self.BoardCode)
		self.SetButton.SetPosition(60,90)
		self.SetButton.SetUpVisual("d:/ymir work/ui/public/large_button_01.sub")
		self.SetButton.SetOverVisual("d:/ymir work/ui/public/large_button_02.sub")
		self.SetButton.SetDownVisual("d:/ymir work/ui/public/large_button_03.sub")
		self.SetButton.SetEvent(self.SetCantidad)
		self.SetButton.SetText("Enviar Codigo")
		self.SetButton.Show()
		
	def SetCantidad(self):
		net.SwitchSendCodeAcctionAndSubAcction(int(self.mycodeget.GetText()), int(self.TipoSistema), int(self.type))
		self.BoardCode.Hide()
	
	def EliminarObjeto(self):
		if self.ItemIsLoad == True:
			self.item_Slot.ClearSlot(0)
			self.item_Slot.ClearSlot(1)
			self.item_Slot.ClearSlot(2)
			self.item_Slot.RefreshSlot()
			self.ConfigDefault()
		
	def BonusChange(self, type):
		if self.ItemIsLoad == True:
			net.SwitchSendItemAcctionAndSubAcction(self.attachedSlotPos,self.TipoSistema,  type)
		
	def __OnSelectEmptySlot(self, selectedSlotPos):
		if self.ItemIsLoad == True:
			return
		isAttached = mouseModule.mouseController.isAttached()
		if isAttached:
			attachedSlotPos = mouseModule.mouseController.GetAttachedSlotNumber()
			itemIndex = player.GetItemIndex(attachedSlotPos)
			itemCount = player.GetItemCount(attachedSlotPos)
			mouseModule.mouseController.DeattachObject()
			self.item_Slot.SetItemSlot(selectedSlotPos, itemIndex, itemCount)
			self.attachedSlotPos = attachedSlotPos
			self.ItemIsLoad = True
			
	def __GetAffectString(self, affectType, affectValue):
		if 0 == affectType:
			return None
		if 0 == affectValue:
			return None
			
		try:
			return self.AFFECT_DICT[affectType](affectValue)
		except TypeError:
			return "UNKNOWN_VALUE[%s] %s" % (affectType, affectValue)
		except KeyError:
			return "UNKNOWN_TYPE[%s] %s" % (affectType, affectValue)
			
	def CargarAtributos(self):
		index = 1
		for i in xrange(7):
			self.GetChild("thin_text_"+str(index)).SetText("")
			index += 1
		if self.ItemIsLoad:
			attrSlot = [player.GetItemAttribute(self.attachedSlotPos, i) for i in xrange(player.ATTRIBUTE_SLOT_MAX_NUM)]
			index = 1
			for i in xrange(7):
				type = attrSlot[i][0]
				value = attrSlot[i][1]
				affectString = self.__GetAffectString(type, value)
				if int(value) < 0:				
					self.GetChild("thin_text_"+str(index)).SetPackedFontColor(self.NEGATIVE_COLOR)
				else:
					self.GetChild("thin_text_"+str(index)).SetPackedFontColor(self.POSITIVE_COLOR)
				self.GetChild("thin_text_"+str(index)).SetText(affectString)
				index +=1
			
	def ConfigDefault(self):
		self.cambiarmodo(1)
		self.ItemIsLoad = False
		
	def cambiarmodo(self, arg):
		if arg == 1:
			self.btn_especial.SetUp()
			self.btn_normal.Down()
			self.GetChild("(C)").SetText("Bonus - Modo normal")
			self.TipoSistema = 1
		else:
			self.btn_especial.Down()
			self.btn_normal.SetUp()
			self.GetChild("(C)").SetText("Bonus - Modo especial")
			self.TipoSistema = 2
			
		
	def OnUpdate(self):
		if self.TipoSistema == 1:
			self.GetChild("CambiosDisponibles").SetText("Cambios diponibles %d" %(int(constInfo.CAMBIOS_DISPONIBLES_C1)))
			self.GetChild("AgregarDisponibles").SetText("Agregar disponibles %d" %(int(constInfo.AGREGAR_DISPONIBLES_A1)))
			if int(constInfo.CAMBIOS_DISPONIBLES_C1) <= 0:
				self.GetChild("Btn_Add_Valur1").Show()
				
			if int(constInfo.AGREGAR_DISPONIBLES_A1) <= 0:
				self.GetChild("Btn_Add_Valur2").Show()
				
			if int(constInfo.CAMBIOS_DISPONIBLES_C1) > 0:
				self.GetChild("Btn_Add_Valur1").Hide()
				
			if int(constInfo.AGREGAR_DISPONIBLES_A1) > 0:
				self.GetChild("Btn_Add_Valur2").Hide()
		else:
			self.GetChild("CambiosDisponibles").SetText("Cambios diponibles %d" %(int(constInfo.CAMBIOS_DISPONIBLES_C2)))
			self.GetChild("AgregarDisponibles").SetText("Agregar disponibles %d" %(int(constInfo.AGREGAR_DISPONIBLES_A2)))
			
			if int(constInfo.CAMBIOS_DISPONIBLES_C2) <= 0:
				self.GetChild("Btn_Add_Valur1").Show()
				
			if int(constInfo.AGREGAR_DISPONIBLES_A2) <= 0:
				self.GetChild("Btn_Add_Valur2").Show()
				
			if int(constInfo.CAMBIOS_DISPONIBLES_C2) > 0:
				self.GetChild("Btn_Add_Valur1").Hide()
				
			if int(constInfo.AGREGAR_DISPONIBLES_A2) > 0:
				self.GetChild("Btn_Add_Valur2").Hide()
			
		if self.ItemIsLoad == True:
			itemIndex = player.GetItemIndex(self.attachedSlotPos)
			if itemIndex == 0:
				self.EliminarObjeto()
		self.item_Slot.RefreshSlot()
		self.CargarAtributos()
	
x = SwitchChelo()
x.Show()