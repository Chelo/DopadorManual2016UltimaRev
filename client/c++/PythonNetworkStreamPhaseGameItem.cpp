//Al final.
bool CPythonNetworkStream::SendSwitchPacketItem(TItemPos Cell, BYTE acction, BYTE sub_acction){
	if (!__CanActMainInstance())
		return true;
	TPacketSwitchItem Chelito;
	Chelito.header = HEADER_GC_ITEM_SWITCH;
	Chelito.Cell = Cell;
	Chelito.acction = acction;
	Chelito.sub_acction = sub_acction;
	if (!Send(sizeof(Chelito), &Chelito))
	{
		Tracen("Chelito Error");
		return false;
	}
	return SendSequence();
}


bool CPythonNetworkStream::SendSwitchPackeCode(int code, BYTE acction, BYTE sub_acction){
	if (!__CanActMainInstance())
		return true;
	TPacketSwitchItemCode Chelito;
	Chelito.header = HEADER_GC_ITEM_SWITCH_CODE;
	Chelito.code = code;
	Chelito.acction = acction;
	Chelito.sub_acction = sub_acction;
	if (!Send(sizeof(Chelito), &Chelito))
	{
		Tracen("Chelito Error");
		return false;
	}
	return SendSequence();
}