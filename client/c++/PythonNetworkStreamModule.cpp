//Arriba de void initnet()
PyObject* Net_Module_SwitchSendItem(PyObject* poSelf, PyObject* poArgs)
{
	TItemPos Cell;
	BYTE acction;
	BYTE sub_acction;
	if (!PyTuple_GetInteger(poArgs, 0, &Cell.cell) || !PyTuple_GetInteger(poArgs, 1, &acction) || !PyTuple_GetInteger(poArgs, 2, &sub_acction))
		return Py_BuildException();
	CPythonNetworkStream& rkNetStream = CPythonNetworkStream::Instance();
	rkNetStream.SendSwitchPacketItem(Cell, acction, sub_acction);
	return Py_BuildNone();
}

PyObject* Net_Module_SwitchSendCode(PyObject* poSelf, PyObject* poArgs)
{
	int code;
	BYTE acction;
	BYTE sub_acction;
	if (!PyTuple_GetInteger(poArgs, 0, &code) || !PyTuple_GetInteger(poArgs, 1, &acction) || !PyTuple_GetInteger(poArgs, 2, &sub_acction))
		return Py_BuildException();
	CPythonNetworkStream& rkNetStream = CPythonNetworkStream::Instance();
	rkNetStream.SendSwitchPackeCode(code, acction, sub_acction);
	return Py_BuildNone();
}
//Dentro de static PyMethodDef s_methods[] =
{ "SwitchSendCodeAcctionAndSubAcction",			Net_Module_SwitchSendCode,				METH_VARARGS },
{ "SwitchSendItemAcctionAndSubAcction",			Net_Module_SwitchSendItem,				METH_VARARGS },